# Bridge remote host:port using docker container

Build a docker container that auto run VPN and create tunnel(s) remote host port to local port (local port is bind between container and host)

```
# ------------------------------------
# Network bind diagram
# ------------------------------------

host:local-port   <--->   container:local-port
    |-- container   <---openvpn--->   remote_network
        |-- container:local-port <---ssh-tunnel--- remote_ssh_host ---> remote_server:remote_port
        
# notes: 
#  - remote_ssh_host is a server run on remote_network
#  - remote_ssh_host can access remote_server
#
# we'll have the result like this:

host:local-port   <--->   remote_server:remote_port
```

with this network setup, we can directly access `remote_server:remote_port` from local host

## Usage note

* Using `data/install_script_example.sh` for reference, create a script file with path `data/install_script.sh`
  * This `data/install_script.sh` will mount to container and auto run everytime container start
* In root folder, run `docker-compose up -d`