#!/bin/bash
# set VPN_USER, VPN_PW
# replace <LOCAL_PORT...>, <REMOTE_SERVER...>, <REMOTE_PORT...>, <OPENVPN_CONFIG_FOLDER>, <VPN_FILE> with real value
# <OPENVPN_CONFIG_FOLDER> is folder contain all file need for VPN, it should have structure like this, example:
#   host_mount
#     | -- openvpn-example-files
#         | -- any-name.ovpn
#         | -- any-name.p12
  #       | -- any-name.key
#

VPN_USER="your_username"
VPN_PW="your_password"
SSH_HOST="ssh_ip_here"
SSH_USER="ssh_username"
SSH_PW="ssh_password"
    
tunnelSSH(){
    # add all port we need to bind remote host here
    # example bind local port 2201 to remote server 192.168.123.1, port 22 -- this remote server must accessible from SSH_HOST
    #   sshpass -p $SSH_PW ssh -oStrictHostKeyChecking=no -g -4 -L 2201:192.168.123.1:22 -f -N $SSH_USER@$SSH_HOST
    sshpass -p $SSH_PW ssh -g -4 -L <LOCAL_PORT1>:<REMOTE_SERVER1>:<REMOTE_PORT1> -f -N $SSH_USER@$SSH_HOST
    sshpass -p $SSH_PW ssh -g -4 -L <LOCAL_PORT2>:<REMOTE_SERVER2>:<REMOTE_PORT2> -f -N $SSH_USER@$SSH_HOST    
}

if [[ ! -f /etc/openvpn/auth.txt ]]
then
    apt-get update && apt-get install nano openvpn ssh sshpass htop iputils-ping netcat telnet -y
    # copy all file need for openvpn run to /etc/openvpn
    cp -R /host_mount/<OPENVPN_CONFIG_FOLDER>/* /etc/openvpn    # eg: cp -R /host_mount/openvpn-example-files/* /etc/openvpn
    echo $VPN_USER > /etc/openvpn/auth.txt
    echo $VPN_PW >> /etc/openvpn/auth.txt
    sed -i "s:#AUTOSTART=\"all\":AUTOSTART=\"all\":g" /etc/default/openvpn
    sed -i "s:auth-user-pass:auth-user-pass auth.txt:g" /etc/openvpn/<VPN_FILE>.ovpn    # eg: sed -i "s:auth-user-pass:auth-user-pass auth.txt:g" /etc/openvpn/any-name.ovpn
    echo "" >> /etc/openvpn/<VPN_FILE>.ovpn
    # change MTU is optional
    echo "tun-mtu 1372" >> /etc/openvpn/<VPN_FILE>.ovpn
    cd /etc/openvpn && nohup openvpn --config /etc/openvpn/<VPN_FILE>.ovpn &
    sleep 5
    mkdir /root/.ssh
    chmod 700 /root/.ssh
    touch /root/.ssh/known_hosts
    chmod 644 /root/.ssh/known_hosts
    ssh-keyscan -H $SSH_HOST >> /root/.ssh/known_hosts
    tunnelSSH
else
    cd /etc/openvpn && nohup openvpn --config /etc/openvpn/<VPN_FILE>.ovpn &
    sleep 5
    ssh-keygen -R $SSH_HOST
    ssh-keyscan -H $SSH_HOST >> /root/.ssh/known_hosts
    tunnelSSH
fi

while true; do
  sleep 10
done